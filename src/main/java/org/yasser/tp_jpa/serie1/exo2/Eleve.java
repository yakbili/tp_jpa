package org.yasser.tp_jpa.serie1.exo2;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name="Eleve")
@Table(name="eleves")
public class Eleve implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(columnDefinition="int", name="id")
	private int id;
	
	@Column(name="nom", length=30)
	private String nom;
	
	@Column(name="age")
	private int age;
	
	@OneToMany
	private Collection<Instruments> instruments ;
	
	public Eleve() {
	}

	public Eleve(String nom, int age) {
		super();
		this.nom = nom;
		this.age = age;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Eleve [nom=" + nom + ", age=" + age + "]";
	}
	
}
