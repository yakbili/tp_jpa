package org.yasser.tp_jpa.serie1.exo2;

import javax.persistence.EntityManager;

public class EleveFactory {
	private EntityManager entityManager;

	public EleveFactory(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}
	
	public Eleve createEleve(String nom , int age) {
		Eleve eleve = new Eleve(nom, age);
		entityManager.getTransaction().begin();
		entityManager.persist(eleve);
		entityManager.getTransaction().commit();
		
		return eleve;
	}
	
	public Eleve find(int id) {
		Eleve eleve = entityManager.find(Eleve.class, id);
		return eleve;
	}
	
	public void delete(int id) {
		entityManager.getTransaction().begin();
		entityManager.remove(find(id));
		entityManager.getTransaction().commit();
	}
}
