package org.yasser.tp_jpa.serie1.exo2;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity(name="Professeur")
@Table(name="professeurs")
public class Professeur implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE) @Column(columnDefinition="int")
	private int id;
	
	@Column(name="nom", length=30)
	private String nom;
	
	@Column(name="age")
	private int age;
	
	
	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.REMOVE})
	@JoinColumn(name="instrument")
	private Instruments instrument;
	
	public Professeur() {
	}
	public Professeur(String nom, int age) {
		super();
		this.nom = nom;
		this.age = age;
	}
	public Professeur(String nom, int age, Instruments instrument) {
		super();
		this.nom = nom;
		this.age = age;
		this.instrument = instrument;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Instruments getInstrument() {
		return instrument;
	}
	public void setInstrument(Instruments instrument) {
		this.instrument = instrument;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "Professeur [nom=" + nom + ", age=" + age + "]";
	}
}
