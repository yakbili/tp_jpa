package org.yasser.tp_jpa.serie1.exo2;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {
	public static void main(String[] args) {
		EntityManagerFactory entityManagerFactory = 
				Persistence.createEntityManagerFactory("jpatest");

		EntityManager em = entityManagerFactory.createEntityManager();

	    //*******************************************************
		Instruments instrument = new Instruments();
		instrument.setNom("Saxophone");
		
		instrument.setPrix(1000);
		instrument.setLocation("O");
		instrument.setPrixCours(15);
		

		Professeur victor = new Professeur("victor", 38);
		victor.setInstrument(instrument);

		//*******************************************************
		em.getTransaction().begin();
		em.persist(victor);
		em.getTransaction().commit();

		//*******************************************************
		Eleve adrien = new Eleve("Adrien", 19);
		em.getTransaction().begin();
		em.persist(adrien);
		em.getTransaction().commit();
	}
}
