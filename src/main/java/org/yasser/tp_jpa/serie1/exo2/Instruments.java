package org.yasser.tp_jpa.serie1.exo2;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity(name="Instruments")
@Table(name="instruments")
public class Instruments implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE) @Column(columnDefinition="int")
	private int id;
	
	@Column(name="nom", length=40)
	private String nom;
	
	@Column(name="prix")
	private int prix;
	
	@Column(name="prix_cours")
	private int prixCours;
	
	@Column(name="location", columnDefinition="char", length=1)
	private String location;
	
	@OneToOne(mappedBy="instrument")
	private Professeur professeur;
	
	public Instruments() {
	}
	public Instruments(String nom, int prix, int prixCours, String location) {
		super();
		this.nom = nom;
		this.prix = prix;
		this.prixCours = prixCours;
		this.location = location;
	}
	public Instruments(String nom, int prix) {
		super();
		this.nom = nom;
		this.prix = prix;
		this.prixCours = 0;
		this.location = "";
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getPrix() {
		return prix;
	}
	public void setPrix(int prix) {
		this.prix = prix;
	}
	public int getPrixCours() {
		return prixCours;
	}
	public void setPrixCours(int prixCours) {
		this.prixCours = prixCours;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	@Override
	public String toString() {
		return "Instruments [nom=" + nom + ", prix=" + prix + ", prixCours=" + prixCours + ", location=" + location
				+ "]";
	}
}
